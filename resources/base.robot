*** Settings ***
Library    Collections
Library    RequestsLibrary
Library    Zoomba.APILibrary
Library    JSONLibrary
Library    String

*** Variables ***

${authUrl}    https://allegro.pl.allegrosandbox.pl/auth/oauth
${clientId}    b1790c04459640bdad1291135e2e110f
${clientSecret}    KI0vGRUddEknWf4JCzDlTlDvjQENnP2T1zfqYUC4eC3u3XJbqAnqJXbpeyV0Hwhf
${TOKEN}    unset

*** Keywords ***
Get Token
    
    Create Session    auth    ${authUrl}    disable_warnings=1        

    ${credentials}=    Evaluate    base64.b64encode('${clientId}:${clientSecret}'.encode("ascii", "ignore"))    modules=base64    

    ${data}=    Create Dictionary    grant_type=client_credentials

    ${header}=    Create Dictionary    Authorization=Basic ${credentials}

    ${resp}=    Post Request    auth    /token?grant_type=client_credentials        headers=${header}        

    Should Be Equal As Strings  ${resp.status_code}     200

    ${authToken}=    Collections.Get From Dictionary    ${resp.json()}    access_token

    Set Suite Variable    ${TOKEN}    ${authToken}    
    