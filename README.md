Solution is prepared with use of  Robot Framework (https://robotframework.org/)

Following external Robot Framework liberies are needed:

- JSONLibrary (https://github.com/robotframework-thailand/robotframework-jsonlibrary)
- Zoomba.APILibrary (https://pypi.org/project/robotframework-zoomba/)

To execute tests please use folowing commend

`Robot tests`

in main directory.



