*** Settings ***
Resource    ../resources/base.robot
Suite Setup    Get Token    
Test Teardown    Delete All Sessions

*** Variables ***
${API_ADRESS}    https://api.allegro.pl.allegrosandbox.pl/sale


** Test Cases ***
Get Without Authorization
    [Documentation]    Test case validate if Get IDs of Allegro categories requiers authorization
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1

    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json        

    ${resp}=    Get Request    allegro    /categories    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     401

    
Get Main Allegro Categories
    [Documentation]    Test case validates if main Allegro categories are returned when parent.id parameter is omitted.
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}    
    
    ${resp}=    Get Request    allegro    /categories    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     200

    ${resultJson}=    To Json    ${resp.content}    pretty_print=True
    
    ${expectedJson}=    Load JSON From File    ./data/Allegro Main Categories.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}
    
Get Motoring Category
    [Documentation]    Test case validetes if subcategories are returned when parent.id parameter is provided with correct (existing) value.
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}    

    ${data}=    Create Dictionary      parent.id=3    

    ${resp}=    Get Request    allegro    /categories    headers=${headers}    data=${data}
    
    Should Be Equal As Strings  ${resp.status_code}     200

    ${resultJson}=    To Json    ${resp.content}    pretty_print=True    

    ${expectedJson}=    Load JSON From File    ./data/Allegro Motoring Categories.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}
    
Get Not Existing Category
    [Documentation]    Test case validetes if correct HTTP status is returned when parent.id parameter is provided with incorrect (not existing) value.
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}
    
    ${data}=    Create Dictionary      parent.id=123-123    

    ${resp}=    Get Request    allegro    /categories    headers=${headers}    data=${data}
    
    Should Be Equal As Strings  ${resp.status_code}     404

    ${resultJson}=    To Json    ${resp.content}    pretty_print=True    

    ${expectedJson}=    Load JSON From File    ./data/Allegro Not Existing Category.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}