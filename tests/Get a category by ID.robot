*** Settings ***
Resource    ../resources/base.robot
Suite Setup    Get Token    
Test Teardown    Delete All Sessions

*** Variables ***
${API_ADRESS}    https://api.allegro.pl.allegrosandbox.pl/sale
${KID_CATEGORY_ID}    11763

** Test Cases ***
Get Kid Category
    [Documentation]    Test case validates if correc data about KID category is returned
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1        
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}    

    ${resp}=    Get Request    allegro    /categories/${KID_CATEGORY_ID}    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     200
    
    ${resultJson}=    To Json    ${resp.content}    pretty_print=True

    ${expectedJson}=    Load JSON From File    ./data/Allegro Kid Category.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}
    
Get Kid Category without authorization
    [Documentation]    Test validates if it is possible to gat data about category without authorization.
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    

    ${resp}=    Get Request    allegro    /categories/${KID_CATEGORY_ID}    headers=${headers}    
    
    Should Be Equal As Strings  ${resp.status_code}     401
    
Get Not Existing Category
    [Documentation]    Test case validates if any data is returned from API if not existing categori is provided in request.
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}    
    
    ${kidCategoryId}=    Set Variable    123-123

    ${resp}=    Get Request    allegro    /categories/${kidCategoryId}    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     404
    
    ${resultJson}=    To Json    ${resp.content}    pretty_print=True    

    ${expectedJson}=    Load JSON From File    ./data/Allegro Not Existing Category.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}