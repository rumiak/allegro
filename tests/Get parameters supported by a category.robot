*** Settings ***
Resource    ../resources/base.robot
Suite Setup    Get Token    
Test Teardown    Delete All Sessions

*** Variables ***
${API_ADRESS}    https://api.allegro.pl.allegrosandbox.pl/sale
${MACHINES_CATEGORY_ID}    252942

** Test Cases ***
Get Parameters For Machines Category
    [Documentation]    Test validate if correct parameters are returned form API for Machines category
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}
    
    ${resp}=    Get Request    allegro    /categories/${MACHINES_CATEGORY_ID}/parameters    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     200
    
    ${resultJson}=    To Json    ${resp.content}    pretty_print=True

    ${expectedJson}=    Load JSON From File    ./data/Allegro Machines Category Parameters.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}

Get Parameters For Machines Category Without Authorization
    [Documentation]    Test validate if it is possible to get parameters for category without authorization
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    
    
    ${resp}=    Get Request    allegro    /categories/${MACHINES_CATEGORY_ID}/parameters    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     401
    
Get Parameters For Existing Category
    [Documentation]    Test validate if it is possible to get parameters for not existing category
    Create Session    allegro    ${API_ADRESS}    disable_warnings=1    
                                 
    ${headers}=   Create Dictionary      accept=application/vnd.allegro.public.v1+json    Authorization=Bearer ${TOKEN}
    
    ${machines_category_id}    Set Variable    123-123

    ${resp}=    Get Request    allegro    /categories/${machines_category_id}/parameters    headers=${headers}
    
    Should Be Equal As Strings  ${resp.status_code}     404
    
    ${resultJson}=    To Json    ${resp.content}    pretty_print=True

    ${expectedJson}=    Load JSON From File    ./data/Allegro Not Existing Category.json
    
    Validate Response Contains Expected Response    ${resultJson}     ${expectedJson}